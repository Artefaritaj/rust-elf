extern crate byteorder;

pub mod types;
#[macro_use]
pub mod utils;

pub mod elf_format;
pub mod errors;

//reexports
pub use elf_format::*;
pub use errors::*;

#[cfg(test)]
mod tests {
    use std::path::PathBuf;
    use elf_file::ElfFile;

    #[test]
    fn test_open_path() {
        let file = ElfFile::open_path(PathBuf::from("tests/samples/test1"))
            .expect("Open test1");
        let bss = file.get_section(".bss").expect("Get .bss section");
        assert!(bss.data.iter().all(|&b| b == 0));
    }
}
