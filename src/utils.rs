#[macro_export]
macro_rules! read_u16 {
    ($data:ident, $io:ident) => ({
        use byteorder::{LittleEndian, BigEndian, ReadBytesExt};
        use types;
        match $data {
            types::ELFDATA2LSB => { $io.read_u16::<LittleEndian>() }
            types::ELFDATA2MSB => { $io.read_u16::<BigEndian>() }
            types::ELFDATANONE => { panic!("Unable to resolve file endianness"); }
            _ => { panic!("Unable to resolve file endianness"); }
        }
    });
}

#[macro_export]
macro_rules! read_u32 {
    ($data:ident, $io:ident) => ({
        use byteorder::{LittleEndian, BigEndian, ReadBytesExt};
        use types;
        match $data {
            types::ELFDATA2LSB => { $io.read_u32::<LittleEndian>() }
            types::ELFDATA2MSB => { $io.read_u32::<BigEndian>() }
            types::ELFDATANONE => { panic!("Unable to resolve file endianness"); }
            _ => { panic!("Unable to resolve file endianness"); }
        }
    });
}

#[macro_export]
macro_rules! read_u64 {
    ($data:ident, $io:ident) => ({
        use byteorder::{LittleEndian, BigEndian, ReadBytesExt};
        use types;
        match $data {
            types::ELFDATA2LSB => { $io.read_u64::<LittleEndian>() }
            types::ELFDATA2MSB => { $io.read_u64::<BigEndian>() }
            types::ELFDATANONE => { panic!("Unable to resolve file endianness"); }
            _ => { panic!("Unable to resolve file endianness"); }
        }
    });
}

#[macro_export]
macro_rules! write_u16 {
    ($data:ident, $io:ident, $n:expr) => ({
        use byteorder::{LittleEndian, BigEndian, WriteBytesExt};
        use types;
        match $data {
            types::ELFDATA2LSB => { $io.write_u16::<LittleEndian>($n as u16) }
            types::ELFDATA2MSB => { $io.write_u16::<BigEndian>($n as u16) }
            types::ELFDATANONE => { panic!("Unable to resolve file endianness"); }
            _ => { panic!("Unable to resolve file endianness"); }
        }
    });
}

#[macro_export]
macro_rules! write_u32 {
    ($data:ident, $io:ident, $n:expr) => ({
        use byteorder::{LittleEndian, BigEndian, WriteBytesExt};
        use types;
        match $data {
            types::ELFDATA2LSB => { $io.write_u32::<LittleEndian>($n as u32) }
            types::ELFDATA2MSB => { $io.write_u32::<BigEndian>($n as u32) }
            types::ELFDATANONE => { panic!("Unable to resolve file endianness"); }
            _ => { panic!("Unable to resolve file endianness"); }
        }
    });
}

#[macro_export]
macro_rules! write_u8 {
    ($io:ident, $n:expr) => ({
        use byteorder::WriteBytesExt;
        $io.write_u8($n as u8)
    });
}

#[macro_export]
macro_rules! write_u64 {
    ($data:ident, $io:ident, $n:expr) => ({
        use byteorder::{LittleEndian, BigEndian, WriteBytesExt};
        use types;
        match $data {
            types::ELFDATA2LSB => { $io.write_u64::<LittleEndian>($n as u64) }
            types::ELFDATA2MSB => { $io.write_u64::<BigEndian>($n as u64) }
            types::ELFDATANONE => { panic!("Unable to resolve file endianness"); }
            _ => { panic!("Unable to resolve file endianness"); }
        }
    });
}

use std;
pub fn get_string(data: &[u8], start: usize) -> Result<String, std::string::FromUtf8Error> {
    let mut end: usize = 0;
    for i in start..data.len() {
        if data[i] == 0u8 {
            end = i;
            break;
        }
    }
    let mut rtn = String::with_capacity(end - start);
    for i in start..end {
        rtn.push(data[i] as char);
    }
    Ok(rtn)
}
