

use errors::*;
use types;
use utils;
use {FileHeader, ProgramHeader, Section, SectionHeader};

use std::fmt;
use std::path::Path;
use std::fs;
use std::io;

pub struct ElfFile {
    pub ehdr: FileHeader,
    pub phdrs: Vec<ProgramHeader>,
    pub sections: Vec<Section>,
}

impl fmt::Debug for ElfFile {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{:?} {:?} {:?}", self.ehdr, self.phdrs, self.sections)
    }
}

impl fmt::Display for ElfFile {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        try!(write!(f, "{{ {} }}", self.ehdr));
        try!(write!(f, "{{ "));
        for phdr in self.phdrs.iter() {
            try!(write!(f, "{}", phdr));
        }
        try!(write!(f, " }} {{ "));
        for shdr in self.sections.iter() {
            try!(write!(f, "{}", shdr));
        }
        write!(f, " }}")
    }
}

impl ElfFile {
    pub fn open_path<T: AsRef<Path>>(path: T) -> Result<ElfFile, ParseError> {
        // Open the file for reading
        let mut io_file = try!(fs::File::open(path));

        ElfFile::open_stream(&mut io_file)
    }

    pub fn open_stream<T: io::Read + io::Seek>(io_file: &mut T) -> Result<ElfFile, ParseError> {
        //Parse the file header
        let ehdr = FileHeader::parse(io_file)?;
        // let data = ehdr.data;

        // Parse the program headers
        // Place cursor to head of program headers table
        try!(io_file.seek(io::SeekFrom::Start(ehdr.phoff)));

        // For each program header
        let mut phdrs: Vec<ProgramHeader> = Vec::new();
        for _ in 0..ehdr.phnum {
            let ph = ProgramHeader::parse(io_file, &ehdr)?;
            phdrs.push(ph);
        }

        // Parse the section headers
        // Place cursor to head of section headers table
        try!(io_file.seek(io::SeekFrom::Start(ehdr.shoff)));

        // For each section header
        let mut shs: Vec<SectionHeader> = Vec::new();
        for _ in 0..ehdr.shnum {
            let sh = SectionHeader::parse(io_file, &ehdr)?;
            shs.push(sh);
        }

        // Read the section data
        // For each section header
        let mut sections: Vec<Section> = Vec::new();
        for shdr in shs {
            let off = shdr.offset;
            let size = shdr.size;

            // Cursor set to section data
            try!(io_file.seek(io::SeekFrom::Start(off)));

            // Read data
            let mut data = vec![0; size as usize];
            if shdr.shtype != types::SHT_NOBITS {
                try!(io_file.read_exact(&mut data));
            }

            //name (string) cannot be known at this step.
            // First we have to parse all sections to find THE section containing names
            sections.push( Section { shdr: shdr, data: data, name: String::new() });
        }

        // :( CLONE!!! to please the borrow checker
        let data_name_section = sections[ehdr.shstrndx as usize].data.clone();

        // Search for the name for each section
        for section in sections.iter_mut() {
            // Read the section name (String version) in the data
            let name_str = utils::get_string(   &data_name_section,
                                                section.shdr.name as usize)?;
            section.name = name_str;
        }

        Ok(ElfFile { ehdr, phdrs, sections })
    }

    pub fn get_symbols(&self, section: &Section) -> Result<Vec<types::Symbol>, ParseError> {
        let mut symbols = Vec::new();
        if section.shdr.shtype == types::SHT_SYMTAB || section.shdr.shtype == types::SHT_DYNSYM {
            let link = &self.sections[section.shdr.link as usize].data;
            let mut io_section = io::Cursor::new(&section.data);
            while (io_section.position() as usize) < section.data.len() {
                try!(self.parse_symbol(&mut io_section, &mut symbols, link));
            }
        }
        Ok(symbols)
    }

    fn parse_symbol(&self, io_section: &mut io::Read, symbols: &mut Vec<types::Symbol>, link: &[u8]) -> Result<(), ParseError> {
        let name: u32;
        let value: u64;
        let size: u64;
        let shndx: u16;
        let mut info = [0u8];
        let mut other = [0u8];

        let data = self.ehdr.data;

        if self.ehdr.class == types::ELFCLASS32 {
            name = try!(read_u32!(data, io_section));
            value = try!(read_u32!(data, io_section)) as u64;
            size = try!(read_u32!(data, io_section)) as u64;
            try!(io_section.read_exact(&mut info));
            try!(io_section.read_exact(&mut other));
            shndx = try!(read_u16!(data, io_section));
        } else {
            name = try!(read_u32!(data, io_section));
            try!(io_section.read_exact(&mut info));
            try!(io_section.read_exact(&mut other));
            shndx = try!(read_u16!(data, io_section));
            value = try!(read_u64!(data, io_section));
            size = try!(read_u64!(data, io_section));
        }

        symbols.push(types::Symbol {
                name:    try!(utils::get_string(link, name as usize)),
                value:   value,
                size:    size,
                shndx:   shndx,
                symtype: types::SymbolType(info[0] & 0xf),
                bind:    types::SymbolBind(info[0] >> 4),
                vis:     types::SymbolVis(other[0] & 0x3),
            });
        Ok(())
    }

    pub fn get_section<T: AsRef<str>>(&self, name: T) -> Option<&Section> {
        self.sections
            .iter()
            .find(|section| section.name == name.as_ref() )
    }

    pub fn get_section_mut<T: AsRef<str>>(&mut self, name: T) -> Option<&mut Section> {
        self.sections
            .iter_mut()
            .find(|section| section.name == name.as_ref() )
    }
}
