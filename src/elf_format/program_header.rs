
use types::*;
use errors::ParseError;
use FileHeader;

use std::fmt;
use std::io;

/// Encapsulates the contents of an ELF Program Header
///
/// The program header table is an array of program header structures describing
/// the various segments for program execution.
#[derive(Copy, Clone, Debug, PartialEq, Eq)]
pub struct ProgramHeader {
    /// Program segment type
    pub progtype: ProgType,
    /// Offset into the ELF file where this segment begins
    pub offset:   u64,
    /// Virtual adress where this segment should be loaded
    pub vaddr:    u64,
    /// Physical address where this segment should be loaded
    pub paddr:    u64,
    /// Size of this segment in the file
    pub filesz:   u64,
    /// Size of this segment in memory
    pub memsz:    u64,
    /// Flags for this segment
    pub flags:    ProgFlag,
    /// file and memory alignment
    pub align:    u64,
}

impl fmt::Display for ProgramHeader {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Program Header: Type: {} Offset: {:#010x} VirtAddr: {:#010x} PhysAddr: {:#010x} FileSize: {:#06x} MemSize: {:#06x} Flags: {} Align: {:#x}",
            self.progtype, self.offset, self.vaddr, self.paddr, self.filesz,
            self.memsz, self.flags, self.align)
    }
}

impl ProgramHeader {

    /// Write this header into a stream. The header must be complete and final.
    pub fn write<T: io::Write>(&self, file_header: &FileHeader, io_file: &mut T) -> io::Result<()> {
        let data = file_header.data;
        let class = file_header.class;

        write_u32!(data, io_file, self.progtype.0)?;

        if class == ELFCLASS32 {
            write_u32!(data, io_file, self.offset)?;
            write_u32!(data, io_file, self.vaddr)?;
            write_u32!(data, io_file, self.paddr)?;
            write_u32!(data, io_file, self.filesz)?;
            write_u32!(data, io_file, self.memsz)?;
            write_u32!(data, io_file, self.flags.0)?;
            write_u32!(data, io_file, self.align)?;
        }
        else {//64
            write_u32!(data, io_file, self.flags.0)?;
            write_u64!(data, io_file, self.offset)?;
            write_u64!(data, io_file, self.vaddr)?;
            write_u64!(data, io_file, self.paddr)?;
            write_u64!(data, io_file, self.filesz)?;
            write_u64!(data, io_file, self.memsz)?;
            write_u64!(data, io_file, self.align)?;
        }

        Ok(())
    }

    pub fn parse<T: io::Read + io::Seek>(io_file: &mut T, file_header: &FileHeader) -> Result<ProgramHeader, ParseError> {
        let progtype: ProgType;
        let offset: u64;
        let vaddr: u64;
        let paddr: u64;
        let filesz: u64;
        let memsz: u64;
        let flags: ProgFlag;
        let align: u64;

        let data = file_header.data;
        let class = file_header.class;

        progtype = ProgType(read_u32!(data, io_file)?);
        if class == ELFCLASS32 {
            offset = read_u32!(data, io_file)? as u64;
            vaddr = read_u32!(data, io_file)? as u64;
            paddr = read_u32!(data, io_file)? as u64;
            filesz = read_u32!(data, io_file)? as u64;
            memsz = read_u32!(data, io_file)? as u64;
            flags = ProgFlag(read_u32!(data, io_file)?);
            align = read_u32!(data, io_file)? as u64;
        } else {
            flags = ProgFlag(read_u32!(data, io_file)?);
            offset = read_u64!(data, io_file)?;
            vaddr = read_u64!(data, io_file)?;
            paddr = read_u64!(data, io_file)?;
            filesz = read_u64!(data, io_file)?;
            memsz = read_u64!(data, io_file)?;
            align = read_u64!(data, io_file)?;
        }

        Ok(ProgramHeader { progtype, offset, vaddr, paddr, filesz, memsz, flags, align})
    }
}
