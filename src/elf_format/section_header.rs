
use types::*;
use FileHeader;
use errors::ParseError;

use std::fmt;
use std::io;

/// Encapsulates the contents of an ELF Section Header
#[derive(Clone, Debug, PartialEq, Eq)]
pub struct SectionHeader {
    /// Section Name
    pub name:      u32,
    /// Section Type
    pub shtype:    SectionType,
    /// Section Flags
    pub flags:     SectionFlag,
    /// in-memory address where this section is loaded
    pub addr:      u64,
    /// Byte-offset into the file where this section starts
    pub offset:    u64,
    /// Section size in bytes
    pub size:      u64,
    /// Defined by section type
    pub link:      u32,
    /// Defined by section type
    pub info:      u32,
    /// address alignment
    pub addralign: u64,
    /// size of an entry if section data is an array of entries
    pub entsize:   u64,
}

impl fmt::Display for SectionHeader {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Section Header: Name: {} Type: {} Flags: {} Addr: {:#010x} Offset: {:#06x} Size: {:#06x} Link: {} Info: {:#x} AddrAlign: {} EntSize: {}",
            self.name, self.shtype, self.flags, self.addr, self.offset,
            self.size, self.link, self.info, self.addralign, self.entsize)
    }
}

impl SectionHeader {
    /// Write this header into a stream. The header must be complete and final.
    pub fn write<T: io::Write>(&self, file_header: &FileHeader, io_file: &mut T) -> io::Result<()> {
        let data = file_header.data;
        let class = file_header.class;

        write_u32!(data, io_file, self.name)?;
        write_u32!(data, io_file, self.shtype.0)?;

        if class == ELFCLASS32 {
            write_u32!(data, io_file, self.flags.0)?;
            write_u32!(data, io_file, self.addr)?;
            write_u32!(data, io_file, self.offset)?;
            write_u32!(data, io_file, self.size)?;

            write_u32!(data, io_file, self.link)?;
            write_u32!(data, io_file, self.info)?;

            write_u32!(data, io_file, self.addralign)?;
            write_u32!(data, io_file, self.entsize)?;
        }
        else {//64
            write_u64!(data, io_file, self.flags.0)?;
            write_u64!(data, io_file, self.addr)?;
            write_u64!(data, io_file, self.offset)?;
            write_u64!(data, io_file, self.size)?;

            write_u32!(data, io_file, self.link)?;
            write_u32!(data, io_file, self.info)?;

            write_u64!(data, io_file, self.addralign)?;
            write_u64!(data, io_file, self.entsize)?;
        }

        Ok(())
    }

    pub fn parse<T: io::Read + io::Seek>(io_file: &mut T, file_header: &FileHeader) -> Result<SectionHeader, ParseError> {
        let name: u32;
        let shtype: SectionType;
        let flags: SectionFlag;
        let addr: u64;
        let offset: u64;
        let size: u64;
        let link: u32;
        let info: u32;
        let addralign: u64;
        let entsize: u64;

        let data = file_header.data;
        let class = file_header.class;

        name = read_u32!(data, io_file)?;
        shtype = SectionType(try!(read_u32!(data, io_file)));
        if class == ELFCLASS32 {
            flags = SectionFlag(try!(read_u32!(data, io_file)) as u64);
            addr = try!(read_u32!(data, io_file)) as u64;
            offset = try!(read_u32!(data, io_file)) as u64;
            size = try!(read_u32!(data, io_file)) as u64;
            link = try!(read_u32!(data, io_file));
            info = try!(read_u32!(data, io_file));
            addralign = try!(read_u32!(data, io_file)) as u64;
            entsize = try!(read_u32!(data, io_file)) as u64;
        } else {
            flags = SectionFlag(try!(read_u64!(data, io_file)));
            addr = try!(read_u64!(data, io_file));
            offset = try!(read_u64!(data, io_file));
            size = try!(read_u64!(data, io_file));
            link = try!(read_u32!(data, io_file));
            info = try!(read_u32!(data, io_file));
            addralign = try!(read_u64!(data, io_file));
            entsize = try!(read_u64!(data, io_file));
        }

        Ok(SectionHeader { name, shtype, flags, addr, offset, size, link, info, addralign, entsize })
    }
}
