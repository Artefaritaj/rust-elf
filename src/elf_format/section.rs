
use std::fmt;
use SectionHeader;
use types::*;
use std::io;

#[derive(Debug, Clone)]
pub struct Section {
    pub shdr: SectionHeader,
    pub name: String,
    pub data: Vec<u8>,
}

impl fmt::Display for Section {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.shdr)
    }
}

impl Section {
    /// Write this section data (only). The header must be complete and final.
    pub fn write_data<T: io::Write + io::Seek>(&self, io_file: &mut T) -> io::Result<()> {
        //write data
        //place cursor
        io_file.seek(io::SeekFrom::Start(self.shdr.offset))?;

        //write
        io_file.write_all(&self.data)?;

        Ok(())
    }

    pub fn replace_word(&mut self, data: Data, address: u64, word_size: u8, new_data: u64) -> Result<(), String> {
        let start = self.shdr.addr;
        let size = self.shdr.size;
        let end = start + size;

        if address < start || (address + word_size as u64) > end {
            return Err(format!("Address is not in this section."));
        }

        let start_index = (address - start) as usize;

        if data == ELFDATA2LSB {
            for i in 0..(word_size as usize) {
                self.data[start_index+i] = ((new_data >> (i*8)) & 0xFF) as u8;
            }
        }
        else {//MSB
            for i in 0..(word_size as usize) {
                self.data[start_index+i] = ((new_data >> ((word_size as usize -1-i)*8)) & 0xFF) as u8;
            }
        }

        Ok(())
    }
}
