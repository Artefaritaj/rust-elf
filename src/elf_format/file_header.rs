
use types::*;
use errors::ParseError;

use std::fmt;
use std::io;

/// Encapsulates the contents of the ELF File Header
///
/// The ELF File Header starts off every ELF file and both identifies the
/// file contents and informs how to interpret said contents. This includes
/// the width of certain fields (32-bit vs 64-bit), the data endianness, the
/// file type, and more.
#[derive(Copy, Clone, Debug, PartialEq, Eq)]
pub struct FileHeader {
    /// 32-bit vs 64-bit
    pub class:      Class,
    /// little vs big endian
    pub data:       Data,
    /// elf version
    pub version:    Version,
    /// OS ABI
    pub osabi:      OSABI,
    /// Version of the OS ABI
    pub abiversion: u8,
    /// ELF file type
    pub elftype:    Type,
    /// Target machine architecture
    pub machine:    Machine,
    /// Virtual address of program entry point
    pub entry:      u64,
    ///Program Header Offset (location in the file)
    pub phoff:      u64,
    ///Section Header Offset (location in the file)
    pub shoff:      u64,
    ///Number of entries in the program header table
    pub phnum:      u16,
    ///Number of entries in the section header table
    pub shnum:      u16,
    ///Index of the section header table entry that contains the section names
    pub shstrndx:   u16,
    /// Interpretation of this field depends on the target architecture
    pub flags:      u32,
    /// Contains the size of this header, normally 64 Bytes for 64-bit and 52 Bytes for 32-bit format.
    pub ehsize:     u16,
    /// Contains the size of a program header table entry.
    pub phentsize:  u16,
    /// Contains the size of a section header table entry.
    pub shentsize:  u16
}

impl FileHeader {

    /// Write this header into a stream. The header must be complete and final.
    pub fn write<T: io::Write>(&self, io_file: &mut T) -> io::Result<()> {
        let data = self.data;
        let class = self.class;

        //write magic
        write_u8!(io_file, ELFMAG0)?;
        write_u8!(io_file, ELFMAG1)?;//E
        write_u8!(io_file, ELFMAG2)?;//L
        write_u8!(io_file, ELFMAG3)?;//F

        // Write platform-independent ident bytes
        write_u8!(io_file, self.class.0)?;
        write_u8!(io_file, self.data.0)?;
        write_u8!(io_file, self.version.0)?;
        write_u8!(io_file, self.osabi.0)?;
        write_u8!(io_file, self.abiversion)?;
        io_file.write_all(&[0u8; 7])?;// write padding
        write_u16!(data, io_file, self.elftype.0)?;
        write_u16!(data, io_file, self.machine.0)?;
        write_u16!(data, io_file, self.version.0)?;

        // Platform dependent header
        if class == ELFCLASS32 {
            write_u32!(data, io_file, self.entry)?;
            write_u32!(data, io_file, self.phoff)?;
            write_u32!(data, io_file, self.shoff)?;
        }
        else {//64
            write_u64!(data, io_file, self.entry)?;
            write_u64!(data, io_file, self.phoff)?;
            write_u64!(data, io_file, self.shoff)?;
        }

        write_u32!(data, io_file, self.flags)?;
        write_u16!(data, io_file, self.ehsize)?;
        write_u16!(data, io_file, self.phentsize)?;
        write_u16!(data, io_file, self.phnum)?;
        write_u16!(data, io_file, self.shentsize)?;
        write_u16!(data, io_file, self.shnum)?;
        write_u16!(data, io_file, self.shstrndx)?;

        Ok(())
    }

    pub fn parse<T: io::Read>(io_file: &mut T) -> Result<FileHeader, ParseError> {
        // Read the platform-independent ident bytes
        let mut ident = [0u8; EI_NIDENT];
        let nread = io_file.read(ident.as_mut())?;

        if nread != EI_NIDENT {
            return Err(ParseError::InvalidFormat(None));
        }

        // Verify the magic number
        if ident[0] != ELFMAG0 || ident[1] != ELFMAG1
                || ident[2] != ELFMAG2 || ident[3] != ELFMAG3 {
            return Err(ParseError::InvalidMagic);
        }

        // Fill in file header values from ident bytes
        let class = Class(ident[EI_CLASS]);
        let data = Data(ident[EI_DATA]);
        let osabi = OSABI(ident[EI_OSABI]);
        let abiversion = ident[EI_ABIVERSION];
        let elftype = Type(read_u16!(data, io_file)?);
        let machine = Machine(read_u16!(data, io_file)?);
        let version = Version(read_u32!(data, io_file)?);

        let phoff: u64;
        let shoff: u64;
        let entry: u64;

        // Parse the platform-dependent file fields
        if class == ELFCLASS32 {
            entry = read_u32!(data, io_file)? as u64;
            phoff = read_u32!(data, io_file)? as u64;
            shoff = read_u32!(data, io_file)? as u64;
        } else {
            entry = read_u64!(data, io_file)?;
            phoff = read_u64!(data, io_file)?;
            shoff = read_u64!(data, io_file)?;
        }

        let flags = try!(read_u32!(data, io_file));
        let ehsize = try!(read_u16!(data, io_file));
        let phentsize = try!(read_u16!(data, io_file));
        let phnum = try!(read_u16!(data, io_file));
        let shentsize = try!(read_u16!(data, io_file));
        let shnum = try!(read_u16!(data, io_file));
        let shstrndx = try!(read_u16!(data, io_file));

        Ok(FileHeader { class, data, osabi, abiversion, elftype, machine,
            version, entry, phoff, shoff, phnum, shnum, shstrndx, flags,
            ehsize, phentsize, shentsize })
    }
}

impl fmt::Display for FileHeader {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "File Header for {} {} Elf {} for {} {}", self.class, self.data,
            self.elftype, self.osabi, self.machine)
    }
}
